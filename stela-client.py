import time
import urllib.request
import json
import serial
import struct
import threading
import sys
import os

#API_KEY = os.getenv("API_KEY")
#BYTE_LEN = int(os.getenv("BYTE_LEN"))
BYTE_LEN = 5
API_KEY = "3:w5I738Dvr3fUeWrSqJBGUe/p4u/W5kWJf5ZAOhkrb5XT"

port_currents = serial.Serial('/dev/ttyUSB_currents', 9600)
port_prices = serial.Serial('/dev/ttyUSB_prices', 2400, bytesize = serial.EIGHTBITS, parity = serial.PARITY_EVEN)
port_currents_fast = serial.Serial('/dev/ttyUSB_currents_fast', 9600)
port_radio = serial.Serial('/dev/ttyUSB_radio', 9600)

currents = [0]*16
prices = [0, 0, 0, 0, 0, 0, 0, 0]
currentsMA = [0]*16

currentTable = [0, 21, 125, 158, 190, 223, 256, 287, 524, 659, 758, 905, 1003, 1392]
adc = [1338, 1424, 1878, 2038, 2211, 2386, 2560, 2745, 3876, 4815, 5520, 6323, 7050, 9474]
channelOffset = [0, 5, 10, -30, -30, 0, 0, 10]
channelOffsetZero = [0, 0, 0, 0, 0, 0, 0, 30]

currentTable2 = [0, 24, 133, 181, 225, 275, 328, 382, 410, 797, 1468, 2045]
adc2 = [1337, 1448, 1926, 2160, 2390, 2650, 2940, 3240, 3410, 5600, 9696, 13600]
channelOffset2 = [0, 20, 20, 10, 35, 50, 20, -30]
channelOffsetZero2 = [0, 0, 0, 0, 0, 50, 0, 0]

currentsRcvFlag = 0

body = {
  "prices": {
    "1": 10,
    "2": 20,
    "3": 30,
    "4": 40
  },
  "currents": {
    "1": 0.1,
    "2": 0.2,
    "3": 0.3,
    "4": 0.4,
    "5": 0.5,
    "6": 0.6,
    "7": 0.7,
    "8": 0.8,
    "9": 0.8,
    "10": 0.8,
    "11": 0.8,
    "12": 0.8,
    "13": 0.8,
    "14": 0.8,
    "15": 0.8,
    "16": 0.8
  }
}

myurl = "https://stelamonitor.ru/api/v1/data/current"
req = urllib.request.Request(myurl)
req.add_header('Content-Type', 'application/json; charset=utf-8')
req.add_header('Authorization', 'Bearer ' + API_KEY)
jsondata = json.dumps(body)
jsondataasbytes = jsondata.encode('utf-8')   # needs to be bytes
req.add_header('Content-Length', len(jsondataasbytes))
print (jsondataasbytes)

def get_currents(port):
	if (port.inWaiting()> BYTE_LEN*10+2):
		if port.read() == b'\x01':
			data_str = port.read(port.inWaiting())
			if data_str[BYTE_LEN*10] == 0x02:
				xor = 0x01
				for i in range(BYTE_LEN*10):
					xor ^= data_str[i]
				xor ^= 0x02
				if xor == data_str[BYTE_LEN*10+1]:
					temp1 = int(data_str[BYTE_LEN*8:BYTE_LEN*(8+1)].decode("utf-8"))*0.03125
					temp2 = int(data_str[BYTE_LEN*9:BYTE_LEN*(9+1)].decode("utf-8"))*0.03125
					for i in range(8):
						currents[i] = int(data_str[BYTE_LEN*i:BYTE_LEN*(i+1)].decode("utf-8"))
						currents[i] = currents[i] + channelOffset[i]
						for j in range(len(adc)-1):
							if currents[i] >= adc[j] and currents[i] < adc[j+1]:
								k = (currentTable[j+1] - currentTable[j])/(adc[j+1] - adc[j])
								currentsMA[i] = k*currents[i] + currentTable[j] - k*adc[j] + 12*(30-temp1)/15
						if currents[i] >= adc[len(adc)-1]:
							k = (currentTable[len(adc)-1] - currentTable[len(adc)-2])/(adc[len(adc)-1] - adc[len(adc)-2])
							currentsMA[i] = k*currents[i] + currentTable[len(adc)-1] - k*adc[len(adc)-1] + 12*(30-temp1)/15
						if currents[i] < adc[0]:
							k = (currentTable[1] - currentTable[0])/(adc[1] - adc[0])
							currentsMA[i] = k*(currents[i] + channelOffsetZero[i])- k*adc[0] + 12*(30-temp1)/15
						body['currents'][str(i+1)] = round(abs(currentsMA[i])/1000, 3)
					#print(temp1, temp2)
					#print(currents)
					#print(body)

def get_currents_2(port):
	if (port.inWaiting()> BYTE_LEN*10+2):
		if port.read() == b'\x01':
			data_str = port.read(port.inWaiting())
			if data_str[BYTE_LEN*10] == 0x02:
				xor = 0x01
				for i in range(BYTE_LEN*10):
					xor ^= data_str[i]
				xor ^= 0x02
				if xor == data_str[BYTE_LEN*10+1]:
					temp1 = int(data_str[BYTE_LEN*8:BYTE_LEN*(8+1)].decode("utf-8"))*0.03125
					temp2 = int(data_str[BYTE_LEN*9:BYTE_LEN*(9+1)].decode("utf-8"))*0.03125
					for i in range(8):
						currents[i+8] = int(data_str[BYTE_LEN*i:BYTE_LEN*(i+1)].decode("utf-8"))
						currents[i+8] = currents[i+8] + channelOffset2[i]
						for j in range(len(adc2)-1):
							if currents[i+8] >= adc2[j] and currents[i+8] < adc2[j+1]:
								k = (currentTable2[j+1] - currentTable2[j])/(adc2[j+1] - adc2[j])
								currentsMA[i+8] = k*currents[i+8] + currentTable2[j] - k*adc2[j] + 12*(30-temp1)/15
						if currents[i+8] >= adc2[len(adc2)-1]:
							k = (currentTable2[len(adc2)-1] - currentTable2[len(adc2)-2])/(adc2[len(adc2)-1] - adc2[len(adc2)-2])
							currentsMA[i+8] = k*currents[i+8] + currentTable2[len(adc2)-1] - k*adc2[len(adc2)-1] + 12*(30-temp1)/15
						if currents[i+8] < adc2[0]:
							k = (currentTable2[1] - currentTable2[0])/(adc2[1] - adc2[0])
							currentsMA[i+8] = k*(currents[i+8] + channelOffsetZero2[i])- k*adc2[0] + 12*(30-temp1)/15
						body['currents'][str(i+1+8)] = round(abs(currentsMA[i+8])/1000, 3)
					#print(temp1, temp2)
					#print(currents)
					#print(body)

def get_prices(port):
	if (port.inWaiting() > 14):
		if port.read() == b'\x02':
			packet = port.read(port.inWaiting())
			if packet[12] == 0x03:
				xor_1 = packet[0] 
				for i in range(1, 13):
					xor_1 ^= packet[i]
				print(xor_1)
				if xor_1 == packet[13]:
					price_num = packet[3] & 0x0F
					print(price_num)
					#if price_num == 5:
						#price_num = 1
					#if price_num == 6:
						#price_num = 2
					#if price_num == 7:
						#price_num = 3
					#if price_num == 8:
						#price_num = 4
					price = 10*(packet[8] & 0x0F) + 1*(packet[9] & 0x0F) + 0.1*(packet[10] & 0x0F) + 0.01*(packet[11] & 0x0F)
					priceX = 1000*(packet[8] & 0x0F) + 100*(packet[9] & 0x0F) + 10*(packet[10] & 0x0F) + 1*(packet[11] & 0x0F)
					print(price)
					if price_num in [1, 2, 3, 4, 5, 6, 7, 8]:
						body['prices'][str(price_num)] = price
						prices[price_num-1] = priceX


def send_currents_prices(port):
	N = 99
	buffer = [0]*N
	buffer[0] = b'\x01'
	for i in range(16):
		zf_number = str(int(round(abs(currentsMA[i])/1000, 4)*1000)).zfill(4)
		for j in range(4):
			buffer[1+i*4+j] = zf_number[j].encode('utf-8')
	for i in range(8):
		zf_number = str(prices[i]).zfill(4)
		for j in range(4):
			buffer[1+4*16+i*4+j] = zf_number[j].encode('utf-8')
	buffer[N-2] = b'\x02'
	xor = 0x01
	for i in range(N-3):
		xor ^= ord(buffer[i+1])
	xor ^= 0x02
	buffer[N-1] = xor.to_bytes(1, 'big')
	for i in range(N):
		port.write(buffer[i])

def send_request():
	jsondata = json.dumps(body)
	jsondataasbytes = jsondata.encode('utf-8')
	req.add_header('Content-Length', len(jsondataasbytes))
	response = urllib.request.urlopen(req, jsondataasbytes, timeout = 3)

t0 = time.time()

while True:
	if (time.time() - t0) > 2.0:
		#print(body)
		send_currents_prices(port_radio)
		t0 = time.time()
		try:
			send_request()
		except:
			print("Error:", sys.exc_info()[0])
		#port_currents_fast.write(b'G')
		#time.sleep(0.01)
	#get_currents(port_currents_fast)
	
	get_currents(port_currents)
	get_prices(port_prices)
	get_currents_2(port_currents_fast)



